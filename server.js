var express = require('express');
var cors = require('cors');
var app = express();
var mysql = require('mysql');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(cors({ origin: true }));
app.use(cors());

const dbConn = mysql.createConnection({
    host: 'se.mfu.ac.th',
    users: 'wd21nakkarin',
    password: 6331305012,
    database: 'wd21_nakkarin_db',
});

dbConn.connect((err) => {
    if (err) throw err;
    console.log('Mysql Connected...');
});

// Run on port http://localhost:7412
app.listen(process.env.PORT || 7412, function () {
    console.log('Node app is running on port 7412');
});
module.exports = app;

////////////////////////////////////////////////////////////////
// products Table
// GET products list products
app.get('/api/products', function (req, res) {
    dbConn.query('SELECT products.product_id, category.category_id, category.category_name, products.product_name, products.product_fullprice, products.product_discount, products.product_totalprice FROM products JOIN category on products.product_category=category.category_id;', function (error, results, fields) {
        try {
            return res.send({ error: false, data: results, message: 'Get All Products' });
        } catch (err) {
            console.log(err);
            return res.send({ error: true, data: results, message: err + "" });
        }
    });
});
// GET products
// Retrieve all productss
// app.get('/api/products', function (req, res) {
//     dbConn.query('SELECT * FROM products', function (error, results, fields) {
//         try {
//             return res.send({ error: false, data: results, message: 'Get All productss' });
//         } catch (err) {
//             console.log(err);
//             return res.send({ error: true, data: results, message: err + "" });
//         }

//     });
// });
// GET by products_id view products
// Retreive products by id ex.http://localhost:7426/api/products/1
app.get('/api/products/:id', function (req, res) {
    let id = req.params.id;
    dbConn.query('SELECT * FROM products WHERE product_id = ? ', [id], function (error, results, fields) {
        try {
            return res.send({ error: false, data: results, message: 'Get Product by ID ' + id });
        } catch (err) {
            console.log(err);
            return res.send({ error: true, data: results, message: err + "" });
        }
    });
});
// POST products
// Add a new products
app.post('/api/products', function (req, res) {
    let product = req.body;
    console.log(req.body);
    dbConn.query("INSERT INTO products SET ? ", product, function (error, results, fields) {
        try {
            console.log(product);
            return res.send({ error: false, data: results, message: 'New Product successfully.' });
        } catch (err) {
            console.log(err);
            return res.send({ error: true, data: results, message: err + "" });
        }
    });
});
// PUT products
// Update products_id
app.put('/api/products/:id', function (req, res) {
    let id = req.params.id;
    let product = req.body;
    // console.log(product);
    dbConn.query("UPDATE products SET ? WHERE product_id=?", [product, id], function (error, results, fields) {
        try {
            console.log(product);
            return res.send({ error: false, data: results, message: 'Update Product '+id });
        } catch (err) {
            console.log(err);
            return res.send({ error: true, data: results, message: err + "" });
        }
    });
});
// DELETE products
// Delete products by id
app.delete('/api/products/:id', function (req, res) {
    let id = req.params.id;
    dbConn.query('DELETE FROM products WHERE product_id = ? ', [id], function (error, results, fields) {
        try {
            return res.send({ error: false, data: results, message: 'Delete Product by ID ' + id });
        } catch (err) {
            console.log(err);
            return res.send({ error: true, data: results, message: err + "" });
        }
    });
});
////////////////////////////////////////////////////////////////
// orders table
// GET orders list orders
app.get('/api/orders', function (req, res) {
    dbConn.query('SELECT orders.order_id, products.product_id, users.user_id, orders.product_totalprice, DATE_FORMAT(orders.order_date, "%d-%m-%Y") AS "order_date" , orders.order_status FROM orders JOIN products ON orders.product_id=products.product_id JOIN users ON users.user_id=orders.user_id;', function (error, results, fields) {
        try {
            return res.send({ error: false, data: results, message: 'Get All Orders' });
        } catch (err) {
            console.log(err);
            return res.send({ error: true, data: results, message: err + "" });
        }
    });
});
//Get orders ID
app.get('/api/orders/:id', function (req, res) {
    let id = req.params.id;
    dbConn.query('SELECT * FROM orders WHERE order_id = ? ', [id], function (error, results, fields) {
        try {
            return res.send({ error: false, data: results, message: 'Get Order by ID ' + id });
        } catch (err) {
            console.log(err);
            return res.send({ error: true, data: results, message: err + "" });
        }
    });
});
//POST orders
app.post('/api/orders', function (req, res) {
    let order = req.body;
    console.log(req.body);
    dbConn.query("INSERT INTO orders SET ? ", order, function (error, results, fields) {
        try {
            console.log(order);
            return res.send({ error: false, data: results, message: 'New Order successfully.' });
        } catch (err) {
            console.log(err);
            return res.send({ error: true, data: results, message: err + "" });
        }
    });
});
//PUT Update orders id 
app.put('/api/orders/:id', function (req, res) {
    let id = req.params.id;
    let order = req.body;
    // console.log(req);
    dbConn.query("UPDATE orders SET ? WHERE order_id=?", [order, id], function (error, results, fields) {
        try {
            console.log(order);
            return res.send({ error: false, data: results, message: 'Update Order '+id });
        } catch (err) {
            console.log(err);
            return res.send({ error: true, data: results, message: err + "" });
        }

    });
});
//DELETE orders id
app.delete('/api/orders/:id', function (req, res) {
    let id = req.params.id;
    dbConn.query('DELETE FROM orders WHERE order_id = ? ', [id], function (error, results, fields) {
        try {
            return res.send({ error: false, data: results, message: 'Delete Order by ID ' + id });
        } catch (err) {
            console.log(err);
            return res.send({ error: true, data: results, message: err + "" });
        }
    });
});
////////////////////////////////////////////////////////////////
// users table
// GET users list users
app.get('/api/users', function (req, res) {
    dbConn.query('SELECT * FROM users', function (error, results, fields) {
        try {
            return res.send({ error: false, data: results, message: 'Get All Users' });
        } catch (err) {
            console.log(err);
            return res.send({ error: true, data: results, message: err + "" });
        }
    });
});
//Get users ID
app.get('/api/users/:id', function (req, res) {
    let id = req.params.id;
    dbConn.query('SELECT * FROM users WHERE user_id = ? ', [id], function (error, results, fields) {
        try {
            return res.send({ error: false, data: results, message: 'Get User by ID ' + id });
        } catch (err) {
            console.log(err);
            return res.send({ error: true, data: results, message: err + "" });
        }
    });
});
//POST users
app.post('/api/users', function (req, res) {
    let user = req.body;
    console.log(req.body);
    dbConn.query("INSERT INTO users SET ? ", user, function (error, results, fields) {
        try {
            console.log(user);
            return res.send({ error: false, data: results, message: 'New User successfully.' });
        } catch (err) {
            console.log(err);
            return res.send({ error: true, data: results, message: err + "" });
        }
    });
});
//PUT Update orders id 
app.put('/api/users/:id', function (req, res) {
    let id = req.params.id;
    let user = req.body;
    dbConn.query("UPDATE users SET ? WHERE user_id=?", [user, id], function (error, results, fields) {
        try {
            console.log(user);
            return res.send({ error: false, data: results, message: 'Update User '+id });
        } catch (err) {
            console.log(err);
            return res.send({ error: true, data: results, message: err + "" });
        }

    });
});
//DELETE orders id
app.delete('/api/users/:id', function (req, res) {
    let id = req.params.id;
    dbConn.query('DELETE FROM users WHERE user_id = ? ', [id], function (error, results, fields) {
        try {
            return res.send({ error: false, data: results, message: 'Delete User by ID ' + id });
        } catch (err) {
            console.log(err);
            return res.send({ error: true, data: results, message: err + "" });
        }
    });
});